<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Edu - Fun</title>
    <link rel="stylesheet" href="style.css">
    <link rel="shortcut icon" href="image/Edu-Fun.png" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
  </head>
  <body style="background-image: url(image/bgg.jpg) ;">
    <nav class="navbar navbar-expand-lg bg-dark">
      <div class="container-fluid">
        <a class="navbar-brand" href="#"><span style="color: aqua;">EDU -</span> <span style= "color: rgb(255, 255, 255);">FUN</span></a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav me-auto mb-2 mb-lg-0">
            <li class="nav-item">
              <a class="nav-link active" aria-current="page" href="#home" style="color: aquamarine;">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" style="color: aquamarine;" href="#about">About</a>
            </li>
          </ul>
          <form class="d-flex" role="search">
            <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success" type="submit">Search</button>
          </form>
        </div>
      </div>
    </nav>
    <br>
    <center>
    <section id="home">
      <div class="jumbotron">
        <h1>EDU - FUN</h1>
        <img src="image/Edu-Fun.png" width="500" height="500">
        <h5>Edu-Fun adalah website edukasi untuk pelajar Sekolah Menengah Atas. Edu-fun menyediakan soal - soal ujian masuk universitas.</h5>
        <h5>Menyediakan beberapa fitur yaitu fitur pengingat waktu belajar, tema gelap, bebahasa indonesia dan fitur waktu isitirahat</h5>
        <a class="btn btn-primary btn-lg" href="login.php" role="button">Login</a>
    </div>
    </section>
    </center>
    <br><br><br><br>
    <center>
      <section id="about">
        <div class="jumbotron">
          <h1>About EDU - FUN</h1>
          <img src="image/Edu-Fun.png" width="500" height="500">
          <h5> Edu-Fun adalah website edukasi untuk pelajar Sekolah Menengah Atas. 
          Edu-fun menyediakan soal-soal ujian masuk universitas dan menyediakan fitur waktu belajar.
          Edu-fun berharap dapat membantu para siswa yang sudah lulus SMA dan ingin masuk universitas. 
          Edu-fun sendiri juga melihat permasalahan dengan teknologi sehingga edu-fun menyediakan fitur istirahat dan waktu belajar untuk menyesuaikan kinerja optimal otak dan kesehatan mata. 
          Selain itu juga, para team edu-fun akan memeriksa setiap materi yang ada seperti materi ujian saintek dan soshum. Admin edu-fun juga akan menyediakan beberapa refrensi soal ujian untuk bisa masuk universitas atau yang biasa disebut soal UTBK.</h5>
      </div>
      </section>
      </center>
  <br>
  <center><div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
      <div class="carousel-item active">

      </div>
    </div>
  </div></center>
  <br><br><br><br><br><br><br>
<section class="team section-padding" id="team">
  <div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="section-header text-center pb-5">
                <h2>Our Team</h2>
                <h4>Berikut profil dari team yang membuat website Edu-Fun.</h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-md-6 col-lg-3">
            <div class="card text-center bg-dark">
                <div class="card-body">
                    <img src="image/raden.jpg" alt="" class="img-fluid rounded-circle" width="200" height="200">
                    <h3 class="card-title py-2" style="color: #fff;">Raden Fadly Kamal Nugraha</h3>
                    <h5 class="card-text"  style="color: #fff;">Ketua Tim</h5>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-6 col-lg-3">
            <div class="card text-center bg-dark">
                <div class="card-body">
                    <img src="image/nanda.jpg" alt="" class="img-fluid rounded-circle" width="200" height="200">
                    <h3 class="card-title py-2" style="color: #fff;">Nanda Dzaky Firnando</h3>
                    <h5 class="card-text"  style="color: #fff;">Anggota 1</h5>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-6 col-lg-3">
            <div class="card text-center bg-dark">
                <div class="card-body">
                    <img src="image/quways.jpg" alt="" class="img-fluid rounded-circle" width="200" height="200">
                    <h3 class="card-title py-2"  style="color: #fff;">Muhammad Quways Al-Qarany</h3>
                    <h5 class="card-text"  style="color: #fff;">Anggota 2</h5>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-6 col-lg-3">
            <div class="card text-center bg-dark">
                <div class="card-body">
                    <img src="image/gangga.jpg" alt="" class="img-fluid rounded-circle" width="200" height="200">
                    <h3 class="card-title py-2"  style="color: #fff;">Gangga Radja <br> Putera</h3>
                    <h5 class="card-text"  style="color: #fff;">Anggota 3</h5>
                </div>
            </div>
        </div>
    </div>
  </div>
</section>
<br><br><br><br><br>
<footer class="text-center text-lg-start bg-dark text-muted">
  <section class="d-flex justify-content-center justify-content-lg-between p-4 border-bottom">

    <div>
      <a href="" class="me-4 text-reset">
        <i class="fab fa-facebook-f"></i>
      </a>
      <a href="" class="me-4 text-reset">
        <i class="fab fa-twitter"></i>
      </a>
      <a href="" class="me-4 text-reset">
        <i class="fab fa-google"></i>
      </a>
      <a href="" class="me-4 text-reset">
        <i class="fab fa-instagram"></i>
      </a>
      <a href="" class="me-4 text-reset">
        <i class="fab fa-linkedin"></i>
      </a>
      <a href="" class="me-4 text-reset">
        <i class="fab fa-github"></i>
      </a>
    </div>
  </section>

  <section class=""
    <div class="container text-center text-md-start mt-5">
      <div class="row mt-3">
        <div class="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">
          <h6 class="text-uppercase fw-bold mb-4">
          <span style="color: aqua;">EDU -</span> <span style="color: white;">FUN</span>
          </h6>
          <p>
            Edu-Fun adalah website edukasi untuk pelajar Sekolah Menengah Atas. Edu-fun menyediakan soal-soal ujian masuk universitas dan menyediakan fitur waktu belajar. Edu-fun berharap dapat membantu para siswa yang sudah lulus SMA dan ingin masuk universitas.
          </p>
        </div>
        <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">
          <h6 class="text-uppercase fw-bold mb-4" style="color: aqua;">
            Products
          </h6>
          <p>
            <a href="#!" class="text-reset">Saintek</a>
          </p>
          <p>
            <a href="#!" class="text-reset">Soshum</a>
          </p>
        </div>
        <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mb-4">
          <h6 class="text-uppercase fw-bold mb-4" style="color: aqua;">
            Useful links
          </h6>
          <p>
            <a href="#!" class="text-reset">Home</a>
          </p>
          <p>
            <a href="#!" class="text-reset">About</a>
          </p>
          <p>
            <a href="#!" class="text-reset">Team</a>
          </p>
          <p>
            <a href="#!" class="text-reset">Login</a>
          </p>
        </div>

        <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4"">
          <h6 class="text-uppercase fw-bold mb-4" style="color: aqua;">Contact</h6>
          <p>Jl. Tegallega RT 01 RW 01</p>
          <p>
            20042002gangga@apps.ipb.ac.id
          </p>
          <p>+6285925095584</p>
        </div>
      </div>
    </div>
  </section>

  <div class="text-center p-4" style="background-color: rgba(0, 0, 0, 0.05);">
    © 2022 Copyright:
    <a class="text-reset fw-bold" href="https://mdbootstrap.com/">Edu-Fun</a>
  </div>
</footer>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js" integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.min.js" integrity="sha384-7VPbUDkoPSGFnVtYi0QogXtr74QeVeeIs99Qfg5YCF+TidwNdjvaKZX19NZ/e6oz" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8" crossorigin="anonymous"></script>
  </body>
</html>