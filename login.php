<html>
	<head>
		<title>Login</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<link rel="shortcut icon" href="image/Edu-Fun.png"/>
		<link rel="stylesheet" href="css/menu.css"/>
		<link rel="stylesheet" href="css/main.css"/>
		<link rel="stylesheet" href="css/bgimg.css"/>
		<link rel="stylesheet" href="css/bgimg-nosocial.css"/>
		<link rel="stylesheet" href="css/font.css"/>
		<link rel="stylesheet" href="css/font-awesome.min.css"/>
		<script type="text/javascript" src="js/jquery-1.12.4.min.js"></script>
		<script type="text/javascript" src="js/main.js"></script>
	</head>
<body style="background: url(image/bg.jpg);">
	<div class="background"></div>
	<div class="backdrop"></div>
	<div class="login-form-container" id="login-form">
		<div class="login-form-content">
			<div class="login-form-header">
				<div class="logo">
					<img src="image/Edu-Fun.png" width="100" height="100">
				</div>
				<h3>Login ke akun Anda</h3>
			</div>
			<form method="post" action="connect2.php" class="login-form">
				<div class="input-container">
					<i class="fa fa-envelope"></i>
					<input type="username" class="input" name="username" placeholder="Username"/>
				</div>
				<div class="input-container">
					<i class="fa fa-lock"></i>
					<input type="password"  id="login-password" class="input" name="password" placeholder="Password"/>
				</div>
				<div class="rememberme-container">
					<input type="checkbox" name="rememberme" id="rememberme"/>
					<label for="rememberme" class="rememberme"><span>Biarkan tetap masuk</span></label>
					<a class="forgot-password" href="#">Lupa Password?</a>
				</div>
				<br><br>
				<center><input type="submit" class="btn">
				<a href="register.php" class="register">Register</a>
			</form>
		</div>
		<div class="attibution">
			&copy; 2022 EDU - FUN
		</div>
	</div>
</body>
</html>