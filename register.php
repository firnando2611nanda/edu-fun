<html>
	<head>
		<title>Register</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<link rel="shortcut icon" href="image/Edu-Fun.png"/>
		<link rel="stylesheet" href="css/menu.css"/>
		<link rel="stylesheet" href="css/main.css"/>
		<link rel="stylesheet" href="css/bgimg.css"/>
		<link rel="stylesheet" href="css/bgimg-nosocial.css"/>
		<link rel="stylesheet" href="css/font.css"/>
		<link rel="stylesheet" href="css/font-awesome.min.css"/>
		<script type="text/javascript" src="js/jquery-1.12.4.min.js"></script>
		<script type="text/javascript" src="js/main.js"></script>
	</head>
<body style="background: url(image/bg.jpg);">
	<div class="background"></div>
	<div class="backdrop"></div>
	<div class="login-form-container" id="login-form">
		<div class="login-form-content">
			<div class="login-form-header">
				<div class="logo">
					<img src="image/Edu-Fun.png" width="100" height="100">
				</div>
				<h1>Silahkan Resgitrasi</h1>
			</div>
			<form class="login-form" action="connect.php" method="post">
                <div class="input-container">
					<input type="text" class="input" id="email" placeholder="Email" name="email"/>
                </div>
				<div class="input-container">
					<input type="text" class="input" id="username" placeholder="Username" name="username"/>
                </div>
                <div class="input-container">	
					<input type="password" class="input" id="password" placeholder="Password" name="password"/>
				</div>
				<div class="input-container">
					<input type="text" class="input" id="asal" placeholder="Asal Sekolah" name="asal"/>
				</div>
				<div class="input-container">
					<input type="text" class="input" id="role" placeholder="Role" name="role"/>
				</div>
				<br><br>
				<center><input type="submit" class="btn">
			</form>
		</div>
		<div class="attibution">
			&copy; 2022 EDU - FUN
		</div>
	</div>
</body>
</html>